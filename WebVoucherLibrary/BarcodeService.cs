﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using ZXing;
using ZXing.Rendering;
using ZXing.Common;

namespace WebVoucherLibrary
{
    public class BarcodeService
    {
        public Bitmap Generate(string code)
        {
            try
            {
                var writer = new BarcodeWriter
                {
                    Format = BarcodeFormat.CODE_128,
                    Options = new EncodingOptions
                    {   Margin = 0,
                        Height = 100,
                        Width = 250
                    },
                    Renderer = new BitmapRenderer()
                };
                return writer.Write(code);
            }
            catch (Exception exc)
            {
                
            }
            return null;
        }
    }
}
