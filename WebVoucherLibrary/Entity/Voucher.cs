﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebVoucherLibrary
{
    public class Voucher
    {
        public Voucher()
        {
            DateCreated = DateTime.Now;
            ExpiryDate = DateCreated.AddYears(1);
        }
        [NotMapped]
        public bool IsExpired
        {
            get
            {
                return DateTime.Now > ExpiryDate;
            }
        }
        [NotMapped]
        public bool IsValid
        {
            get
            {
                return (Id > 0 && !IsExpired);
            }
        }

        [Key]
        public long Id { get; set; }

        [MaxLength(100)]
        public string CreatedBy { get; set; }

        public decimal Value { get; set; }

        public DateTime DateCreated { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime? RedemptionDate { get; set; }
        

        [MaxLength(100)]
        public string RedeemedBy { get; set; }
        [MaxLength(50)] public string Code { get; set; }


        [MaxLength(500)] public string RecipientEmail { get; set; }


    }
}
