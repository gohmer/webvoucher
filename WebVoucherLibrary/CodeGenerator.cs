﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebVoucherLibrary
{
    public class CodeGenerator
    {
        private string charList = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        private Random rnd = new Random();
        public int Obfuscate(int x)
        {
            int y = x / 15 + ((x / 5) % 3) * 4 + (x % 5) * 12;
            return y;
        }
        public string Generate(long id, int length = 4)
        {
            var code = "";
            for (int i = 0; i < length; i++)
            {
                code += RandomCharacter();
            }
            return code + id.ToString("#000");
        }

        private string RandomCharacter()
        {
            return charList.Substring(rnd.Next(1, 25), 1);
        }
    }
}
