﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebVoucherLibrary
{
    public class VoucherService
    {
        private CodeGenerator codeGenerator = new CodeGenerator();
        public Voucher Find(string code)
        {
            using (var db = new VoucherDb())
            {
                var result = db.Vouchers.Where(x => x.Code == code).OrderByDescending(x => x.Id).Take(1).Single();
                return result ?? new Voucher();
            }
            
        }

        public bool Redeem(string code, string name)
        {
            var voucher = Find(code);
            
            using (var db = new VoucherDb())
            {
                var target = db.Vouchers.Find(voucher.Id);
                target.RedemptionDate = DateTime.Now;
                target.RedeemedBy = name;
                db.SaveChanges();
                return true;
            }
        }
        public Voucher Create(Voucher voucher)
        {
            using (var db = new VoucherDb())
            {
                db.Vouchers.Add(voucher);
                db.SaveChanges();
                voucher.Code = codeGenerator.Generate(voucher.Id);
                db.SaveChanges();
                return voucher;
            }
        }
        //public bool Update(Voucher voucher)
        //{
        //    return true;
        //}
    }
}
