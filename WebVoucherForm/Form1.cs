﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebVoucherLibrary;

namespace WebVoucherForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnObfuscate_Click(object sender, EventArgs e)
        {
            int x = int.Parse(txtNumber.Text);
            var codeGen = new CodeGenerator();
            MessageBox.Show(codeGen.Obfuscate(x).ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var voucher = new Voucher
            {
                CreatedBy = "Allen",
                Value = 50.0m
            };
            var voucherService = new VoucherService();
            var newVoucher = voucherService.Create(voucher);
            MessageBox.Show(newVoucher.Code);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Database.SetInitializer(new VoucherDbInitialiser());
        }
    }
}
