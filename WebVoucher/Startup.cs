﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebVoucher.Startup))]
namespace WebVoucher
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
