﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebVoucherLibrary;

namespace WebVoucher.Controllers
{
    public class HomeController : Controller
    {
        private VoucherService voucherService = new VoucherService();
        private BarcodeService barcodeService = new BarcodeService();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult CreateVoucher()
        {
            var imageFolder = Server.MapPath("~/Content/Image");
            var voucher = new Voucher { CreatedBy = "Allen", Value = 50.00m };
            voucherService.Create(voucher);
            Bitmap bitmap = barcodeService.Generate(voucher.Code);
            string filename = string.Format("{0}/{1}.png", imageFolder, voucher.Code);
            bitmap.Save(filename, ImageFormat.Png);
            return View(voucher);

        }
    }
}